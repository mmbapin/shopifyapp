import React, { Component } from 'react'
import axios from 'axios'
import { Layout, TextStyle, Card, Page } from '@shopify/polaris';

class score extends Component {
  state={
    scoreData: null,
    rendergrid: 3
  }

  componentDidMount(){
   this.getData()
  }

   getData = async () => {
    try {
        const resp = await axios.post('https://inference.ai-commerce.dev/score');
        this.setState({ scoreData: resp.data.score })
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
  };
  render() {
    const {scoreData, rendergrid} = this.state
    return (
      <Page>
        {
          scoreData === null ? (
            <TextStyle variation="subdued">Loading</TextStyle>
          ) : (
            <Layout>
               {
              Array.apply(0, Array(3)).map((data, i) => (
                <Layout.Section oneThird key={i}>
                  <Card title={i === 0 && "Smart Grid Opened Percent" || i === 1 && "Smart Grid Converted Percent" || i=== 2 && "Smart Grid Clicked Percent"  } sectioned>
                    <p>{i === 0 && scoreData.smart_grid_opened_percent || i === 1 && scoreData.smart_grid_converted_percent || i ===2 && scoreData.smart_grid_clicked_percent}</p>
                  </Card>
                 
                 </Layout.Section>
              ))
            }
            </Layout>
          )
        }
      </Page>
    )
  }
}

export default score
