import { Layout, Page, SettingToggle, TextStyle } from '@shopify/polaris'
import React, { Component } from 'react'


class install extends Component {
  state={
    isInstall: false
  }


  handleAction = () => {
    console.log("Pressed");
    this.setState({
      isInstall : !this.state.isInstall
    })
  }
  render() {
  const {isInstall} = this.state;  
  const titleDescription = isInstall ? 'Uninstall' : 'Install';
  const bodyDescription = isInstall ? 'installed' : 'uninstalled';

    return (
     <Page>
       <Layout.AnnotatedSection
        title={`${titleDescription} App`}
        description='Toggle app installation on your shop.'
       >
         <SettingToggle
          action={{
            content: titleDescription,
            onAction: this.handleAction
          }}
          enabled={true}
         >
           The app script is <TextStyle variation='strong'>{bodyDescription}</TextStyle>
         </SettingToggle>

       </Layout.AnnotatedSection>
     </Page>
    )
  }
}

export default install
