import React,{Component} from 'react'
import { EmptyState, Heading, Page } from "@shopify/polaris";
import {Provider, ResourcePicker} from '@shopify/app-bridge-react';
import ProductList from '../components/ProductList';
import ProductPage from '../components/ProductPage';
import ProductEmpty from '../components/ProductEmpty';
import Store from "store-js";

class Index extends Component{
  state={
    open: false,
    prevState: [],
    selectedProduct: [],
    productId: []
  }

  componentDidMount(){
    const productList = Store.get(`${this.props.host}-products`);
    if (productList) {
      this.setState({
        selectedProduct: productList
      })
    }
  }


  componentDidUpdate(prevProps, prevState){
   if (prevState.selectedProduct !== this.state.selectedProduct) {
     const ids = this.state.selectedProduct.map((product)=> {
       return{
         id: product.id
       }
     });
     this.setState({
       productId: ids
     })
   } 
  }
 



  handleResouce = () => {
    this.setState({
      open: true
    })
  };

  handleCancle = () => {
    this.setState({
      open: false
    })
  };

  handleSelection = (data) => {
    this.setState({ 
      open : false, 
      selectedProduct : data.selection
    });

    Store.set(`${this.props.host}-products`, data.selection)
  }

  render(){
    const {open, selectedProduct, productId} = this.state
    return(
      <div>
         
        <ResourcePicker 
          resourceType='Product'
          open= {open}
          onCancel={this.handleCancle}
          onSelection={(resources) => this.handleSelection(resources)}
          initialSelectionIds={productId} 
        />

        {
          selectedProduct.length > 0 ? (
        
            <ProductPage handleResource={this.handleResouce} selectedProduct={selectedProduct}/>

          ) : (
           <ProductEmpty handleResouce={this.handleResouce}/>
          )
        }
      </div>
    )
  }
};

export default Index;
