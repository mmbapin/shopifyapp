import React, { Component } from 'react';
import { EmptyState, Heading, Page } from "@shopify/polaris";
import ProductList from './ProductList';

class ProductPage extends Component {
  render() {
    const{handleResource, selectedProduct} = this.props
    return (
      <Page
        title='Product Selector'
        primaryAction={{
          content: 'Product',
          onAction: () => handleResource()
        }}
      >

        <ProductList products={selectedProduct} />

      </Page>
    )
  }
}

export default ProductPage
