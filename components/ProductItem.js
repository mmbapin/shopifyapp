import React, { Component } from 'react'
import { ResourceList, Stack, TextStyle, Thumbnail } from '@shopify/polaris';
import {HideMinor} from '@shopify/polaris-icons';

class ProductItem extends Component {
  render() {
    const{product} = this.props
    const img = product.images[0] ? product.images[0].originalSrc : HideMinor
    const media = <Thumbnail source={img}/>
    const price = product.variants[0].price
    return (
      <ResourceList.Item media={media} id={product.id} accessibilityLabel={`View details for ${product.title}`}>
        <Stack>
          <Stack.Item fill>
            <TextStyle variation='strong'>
              <h4>{product.title}</h4>
            </TextStyle>
          </Stack.Item>

          <Stack.Item>
            <p>${price}</p>
          </Stack.Item>
        </Stack>
      </ResourceList.Item>
    )
  }
}

export default ProductItem
