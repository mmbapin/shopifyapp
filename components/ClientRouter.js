import React, { Component } from 'react'
import {withRouter} from 'next/router';
import { ClientRouter as AppBridgeClientRouter } from '@shopify/app-bridge-react';

class ClientRouter extends Component {
  render() {
    const {router} = this.props
    return (
      <div>
        <AppBridgeClientRouter history={router}/>
      </div>
    )
  }
}

export default withRouter(ClientRouter);


