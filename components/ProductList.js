import React, { Component } from 'react'
import {Card, ResourceList, ResourceItem, TextStyle, Avatar} from "@shopify/polaris"
import ProductItem from './ProductItem'

class ProductList extends Component {
  render() {
    const {products} = this.props
    return (
      <Card>
        <ResourceList 
          showHeader
          resourceName={{singular: 'Product', plural: 'Products'}}
          items={products}
          renderItem={(product) => (
            <ProductItem product={product}/>
          )}
        />
        {/* <ResourceList 
          showHeader
          resourceName={{singular: 'Product', plural: 'Products'}}
          items={products}
          renderItem={(product) => {
            const {title, id, vendor, images, handle} = product;
            const authorMarkup = vendor ? <div>by {vendor}</div> : null;
            return(
              <ResourceItem
              id={id}
              url={handle}
              media={
                <Avatar customer size="medium" name={title} source={images[0].originalSrc} />
              }
              accessibilityLabel={`View details for ${vendor}`}
              name={title}
              
            >
              <h3>
                <TextStyle variation="strong">{title}</TextStyle>
              </h3>
              {authorMarkup}
            </ResourceItem>
            )
          }}
        /> */}
      </Card>
    )
  }
}

export default ProductList
