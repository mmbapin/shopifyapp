import React, { Component } from 'react'
import { EmptyState, Heading, Page } from "@shopify/polaris";


class ProductEmpty extends Component {
  render() {
    const {handleResouce} = this.props
    return (
      <EmptyState
        heading='Manage Products You Want To Display!'
        action={{
          content: 'Select Products',
          onAction: () => handleResouce()
        }}
        image="https://cdn.shopify.com/s/files/1/0262/4071/2726/files/emptystate-files.png"
      >
        <p>Product you want to use on product lists.</p>
      </EmptyState>
    )
  }
}

export default ProductEmpty
